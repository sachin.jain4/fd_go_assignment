package service

import (
	"encoding/base64"
	"fmt"
	"go-crud/dao"
	"go-crud/models"
	pb "go-crud/pb"
	"go-crud/validator"
	"log"
	"strings"

	"google.golang.org/protobuf/proto"
	"gopkg.in/mgo.v2/bson"
)

func handleErr(err error, msg string) {
	if err != nil {
		log.Println(err, msg)
	}
}

func GetUserByUserId(marshalledBody []byte) ([]byte, error) {

	req := pb.CreateUserResponse{}
	err := proto.Unmarshal(marshalledBody, &req)
	handleErr(err, "Error unmarshalling request")

	//Validation
	error := validator.ValidateHexId(req.Id)
	if error != nil {
		errResponse, _ := proto.Marshal(&pb.GetUserResponse{
			Error: error.Error(),
		})
		return errResponse, error
	}

	oid := bson.ObjectIdHex(req.Id)

	u, err := dao.GetUserById(oid)
	if err != nil {
		errResponse, _ := proto.Marshal(&pb.GetUserResponse{
			Error: "no user exist with provided user id",
		})
		return errResponse, err
	}

	e, err := dao.GetEmpById(oid)
	if err != nil {
		errResponse, _ := proto.Marshal(&pb.GetUserResponse{
			Error: "no employee data exist with provided user id",
		})
		return errResponse, err
	}

	userPb := &pb.GetUserResponse{
		FirstName:   string(u.FirstName),
		LastName:    string(u.LastName),
		Email:       string(u.Email),
		Designation: string(e.Designation),
	}

	user, err := proto.Marshal(userPb)
	handleErr(err, "Error unmarshalling response user ")
	if err != nil {
		errResponse, _ := proto.Marshal(&pb.GetUserResponse{
			Error: "Error while marshalling response user data",
		})
		return errResponse, err
	}
	log.Println(user)

	return user, err

}

func GetAllUsers() ([]byte, error) {

	usrs, err := dao.GetAllUsers()
	if err != nil {
		errResponse, _ := proto.Marshal(&pb.GetAllUsersResponse{
			Users: nil,
			Error: err.Error(),
		})
		return errResponse, err
	}

	for i, j := 0, len(usrs)-1; i < j; i, j = i+1, j-1 {
		usrs[i], usrs[j] = usrs[j], usrs[i]
	}

	allUsers := &pb.GetAllUsersResponse{}
	usersArr := []*pb.UserDetails{}
	for i := 0; i < len(usrs); i++ {

		e, err := dao.GetEmpById(usrs[i].Id)
		if err != nil {
			log.Fatalf("emp with userId %s not found", usrs[i].Id)
		}
		u := &pb.UserDetails{
			Id:          usrs[i].Id.Hex(),
			FirstName:   usrs[i].FirstName,
			LastName:    usrs[i].LastName,
			Email:       usrs[i].Email,
			Designation: e.Designation,
			UserId:      e.UserId,
		}
		usersArr = append(usersArr, u)
	}

	allUsers.Users = usersArr

	uj, err := proto.Marshal(allUsers)
	if err != nil {
		errResponse, _ := proto.Marshal(&pb.GetAllUsersResponse{
			Error: "Error while marshalling response data for allUsers",
		})
		return errResponse, err
	}

	return uj, err

}

func CreateUser(marshalledBody []byte) ([]byte, error) {
	u := models.User{}
	e := models.Employee{}
	reqData := pb.CreateUserRequest{}

	err := proto.Unmarshal(marshalledBody, &reqData)
	handleErr(err, "error while unmarshalling request body")

	//Validation
	error := validator.ValidateCreateUserReq(&reqData)
	if error != nil {
		errResponse, _ := proto.Marshal(&pb.CreateUserResponse{
			Error: error.Error(),
		})
		return errResponse, error
	}

	u.Id = bson.NewObjectId()
	u.FirstName = reqData.FirstName
	u.LastName = reqData.LastName
	u.Email = reqData.Email
	error = dao.SaveUser(u)
	if error != nil {
		errResponse, _ := proto.Marshal(&pb.CreateUserResponse{
			Error: "error while saving user data",
		})
		return errResponse, error
	}

	e.Id = u.Id
	e.UserId = strings.Split(reqData.Email, "@")[0]
	e.Designation = reqData.Designation
	error = dao.SaveEmp(e)
	if error != nil {
		errResponse, _ := proto.Marshal(&pb.CreateUserResponse{
			Error: "error while saving employee data",
		})
		return errResponse, error
	}

	cur := &pb.CreateUserResponse{}
	cur.Id = string(u.Id.Hex())
	log.Println("User _Id in string is ", u.Id.Hex())
	log.Println("Proto response of _id is ", cur)
	res, err := proto.Marshal(cur)
	if err != nil {
		errResponse, _ := proto.Marshal(&pb.CreateUserResponse{
			Error: "Error while marshalling response data",
		})
		return errResponse, err
	}
	log.Println("Marshalled body of response _id is ", string(res))

	// logging the base64 proto request for getUserById API
	sEnc := base64.StdEncoding.EncodeToString([]byte(res))
	log.Println("Base64 encoded request of Created _id is ", sEnc)

	return res, nil
}

func UpdateUser(marshalledBody []byte) ([]byte, error) {

	reqData := pb.UpdateUserRequest{}
	err2 := proto.Unmarshal(marshalledBody, &reqData)
	handleErr(err2, "error while unmarshalling request body")

	//Validation
	error := validator.ValidateUpdateUserReq(&reqData)
	if error != nil {
		errResponse, _ := proto.Marshal(&pb.UpdateUserResponse{
			Error: error.Error(),
		})
		return errResponse, error
	}

	e, err := dao.FindEmpByUserId(reqData.UserId)
	if err != nil {
		log.Fatalf("emp with userId %s not found", reqData.UserId)
		errResponse, _ := proto.Marshal(&pb.UpdateUserResponse{
			Error: fmt.Sprintf("emp with userId %s not found", reqData.UserId),
		})
		return errResponse, error
	}

	u, err := dao.GetUserById(e.Id)
	if err != nil {
		log.Fatalf("user with userId %s not found", e.Id)
		errResponse, _ := proto.Marshal(&pb.UpdateUserResponse{
			Error: fmt.Sprintf("user with userId %s not found", e.Id),
		})
		return errResponse, error
	}
	u.Email = reqData.Email

	err3 := dao.UpdateUser(e.Id, *u)
	handleErr(err3, "error while updating user emailId")

	userData := &pb.User{
		FirstName:   u.FirstName,
		LastName:    u.LastName,
		Email:       u.Email,
		Designation: e.Designation,
	}
	res, err := proto.Marshal(userData)
	handleErr(err, "Error while marshalling response data")
	log.Println(res)

	return res, err
}

func DeleteUser(marshalledBody []byte) ([]byte, error) {
	errFlag := false
	errMsg := ""
	req := pb.DeleteUserRequest{}
	err := proto.Unmarshal(marshalledBody, &req)
	handleErr(err, "Error while unmarshalling request body")

	//Validation
	error := validator.ValidateHexId(req.GetId())
	if error != nil {
		errResponse, _ := proto.Marshal(&pb.DeleteUserResponse{
			Msg:   "Fail to delete user",
			Error: error.Error(),
		})
		return errResponse, error
	}

	oid := bson.ObjectIdHex(req.Id)

	err1 := dao.DeleteEmp(oid)
	if err1 != nil {
		errFlag = true
		errMsg = fmt.Sprintf("emp with userId %s not found", oid)
		log.Fatalf("emp with userId %s not found", oid)
	}

	err2 := dao.DeleteUser(oid)
	if err2 != nil {
		errFlag = true
		errMsg = fmt.Sprintf("user with userId %s not found", oid)
		log.Fatalf("user with userId %s not found", oid)
	}

	if errFlag {
		errResponse, _ := proto.Marshal(&pb.DeleteUserResponse{
			Msg:   "Fail to delete user",
			Error: errMsg,
		})
		return errResponse, err2
	}

	errResponse, _ := proto.Marshal(&pb.DeleteUserResponse{
		Msg:   "User deleted successfully",
		Error: "",
	})

	return errResponse, err2
}

func GetEncodedRequest(id string) ([]byte, error) {

	//Validation
	error := validator.ValidateHexId(id)
	if error != nil {
		errResponse, _ := proto.Marshal(&pb.GetEncodedResponse{
			Id:    "",
			Error: error.Error(),
		})
		return errResponse, error
	}

	oid := bson.ObjectIdHex(id)

	cur := &pb.CreateUserResponse{}
	cur.Id = id
	log.Println("User _Id in string is ", oid.Hex())
	log.Println("Proto response of _id is ", cur)
	res, err := proto.Marshal(cur)
	handleErr(err, "Error unmarshalling response user ")
	log.Println("Marshalled body of response _id is ", string(res))

	encodedReq := &pb.GetEncodedResponse{}
	// logging the base64 proto request for getUserById API
	sEnc := base64.StdEncoding.EncodeToString([]byte(res))
	log.Println("Base64 encoded request of Created _id is ", sEnc)

	encodedReq.Id = sEnc
	base64EncodedReq, err := proto.Marshal(encodedReq)
	handleErr(err, "Error unmarshalling response of encoded request ")
	log.Println(base64EncodedReq)

	return base64EncodedReq, err
}
