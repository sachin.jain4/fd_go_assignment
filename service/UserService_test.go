package service_test

import (
	"encoding/base64"
	"fmt"
	"go-crud/dao"
	pb "go-crud/pb"
	"go-crud/service"
	"math/rand"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"google.golang.org/protobuf/proto"
	"gopkg.in/mgo.v2/bson"
)

func TestGetUserByUserId(t *testing.T) {
	assert := assert.New(t)
	//parameterized tests
	testParams := []struct {
		id string
	}{
		{"Chg2MmZiNjU0N2ZmNGM2MGJhN2MyYTkyYjU="},
		{"xyz"},
	}
	for i, tp := range testParams {
		t.Run(fmt.Sprintf("TestGetUserByUserId for input : %d", i), func(t *testing.T) {
			input, _ := base64.StdEncoding.DecodeString(tp.id)
			_, resErr := service.GetUserByUserId(input)
			if i == 0 {
				assert.Equal(nil, resErr)
			} else {
				assert.Equal("The value of 'id' is not a valid Object hexId value.\r\n", resErr.Error())
			}
		})
	}

}

func TestGetAllUsers(t *testing.T) {
	assert := assert.New(t)

	actual, err := service.GetAllUsers()

	assert.Equal(err, nil)

	if err != nil {
		t.Errorf("error while fetching GetAllUser result : Result = %s", actual)
	}
}

func TestCreateUser(t *testing.T) {
	assert := assert.New(t)

	//parameterized tests
	testParams := []struct {
		firstName   string
		lastName    string
		email       string
		designation string
	}{
		{"Test",
			"Case",
			"TestC@reddif.com",
			"SDE",
		},
		{"Test1",
			"Case",
			"TestC@reddif.com",
			"SDE",
		},
		{"Test",
			"Case1",
			"TestC@reddif.com",
			"SDE",
		},
		{"Test",
			"Case",
			"TestCreddif.com",
			"SDE",
		},
		{"Test",
			"Case",
			"TestC@reddif.com",
			"SDE3",
		},
	}

	for i, tp := range testParams {
		t.Run(fmt.Sprintf("TestCreateUser for input : %d", i), func(t *testing.T) {

			reqData := pb.CreateUserRequest{
				FirstName:   tp.firstName,
				LastName:    tp.lastName,
				Email:       tp.email,
				Designation: tp.designation,
			}
			marshalledBody, _ := proto.Marshal(&reqData)

			res, err := service.CreateUser(marshalledBody)

			result := &pb.CreateUserResponse{}
			proto.Unmarshal(res, result)
			switch i {
			case 0:
				assert.Equal(true, bson.IsObjectIdHex(result.GetId()))
			case 1:
				assert.Equal("The value of 'firstName' is not a valid string value.\r\n", err.Error())
			case 2:
				assert.Equal("The value of 'lastName' is not a valid string value.\r\n", err.Error())
			case 3:
				assert.Equal("The value of 'email' is not a valid emailId.\r\n", err.Error())
			case 5:
				assert.Equal("The value of 'designation' is not valid.\r\n", err.Error())
			}
		})
	}
}

func TestUpdateUser(t *testing.T) {
	assert := assert.New(t)
	rand.Seed(time.Now().UnixNano())
	randomNo := rand.Intn(9-5+1) + 5

	//parameterized tests
	testParams := []struct {
		userId string
		email  string
	}{
		{
			"TestC",
			"TestC" + strconv.Itoa(randomNo) + "@reddif.com",
		},
		{
			"TestC@",
			"TestC" + strconv.Itoa(randomNo) + "@reddif.com",
		},
		{
			"TestC",
			"TestCreddif.com",
		},
	}

	for i, tp := range testParams {
		t.Run(fmt.Sprintf("TestUpdateUser for input : %d", i), func(t *testing.T) {

			reqData := pb.UpdateUserRequest{
				UserId: tp.userId,
				Email:  tp.email,
			}
			marshalledBody, _ := proto.Marshal(&reqData)

			res, err := service.UpdateUser(marshalledBody)

			result := &pb.User{}
			proto.Unmarshal(res, result)

			switch i {
			case 0:
				assert.Equal(result.GetEmail(), tp.email)
			case 1:
				assert.Equal("The value of 'userId' is not a valid alphanumeric value.\r\n", err.Error())
			case 2:
				assert.Equal("The value of 'email' is not a valid emailId.\r\n", err.Error())
			}
		})
	}
}

func TestDeleteUser(t *testing.T) {
	assert := assert.New(t)

	e, _ := dao.FindEmpByUserId("TestC")
	testId := e.Id.Hex()

	//parameterized tests
	testParams := []struct {
		id string
	}{
		{testId},
		{"xyz"},
	}
	for i, tp := range testParams {
		t.Run(fmt.Sprintf("TestGetUserByUserId for input : %d", i), func(t *testing.T) {
			req := &pb.DeleteUserRequest{
				Id: tp.id,
			}
			marshalledBody, _ := proto.Marshal(req)
			res, err := service.DeleteUser(marshalledBody)
			actual := &pb.DeleteUserResponse{}
			proto.Unmarshal(res, actual)
			switch i {
			case 0:
				assert.Equal("User deleted successfully", actual.Msg)
			case 1:
				assert.Equal("Fail to delete user", actual.Msg)
				assert.NotEqual(nil, err)
			}
		})
	}
}

func TestGetEncodedRequest(t *testing.T) {
	assert := assert.New(t)

	expected := "62ef5b19ff4c60ea745ed7ab"
	base64EncodedReq, _ := service.GetEncodedRequest(expected)

	request := &pb.GetEncodedRequest{}
	proto.Unmarshal(base64EncodedReq, request)

	res, _ := base64.StdEncoding.DecodeString(request.GetId())
	result := &pb.GetEncodedRequest{}
	proto.Unmarshal(res, result)
	actual := result.GetId()

	assert.Equal(expected, actual)
}
