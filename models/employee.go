package models

import (
	"gopkg.in/mgo.v2/bson"

)

type Employee struct {
	Id          bson.ObjectId `json:"id" bson:"_id"`
	UserId      string        `json:"userId" bson:"userId"`
	Designation string        `json:"designation" bson:"designation"`
}
