package validator

import (
	"errors"
	"fmt"
	pb "go-crud/pb"
	"regexp"
	"strconv"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

func Validate(params map[string]interface{}, validationRules map[string]string) error {

	error := ""

	for field, rules := range validationRules {

		for _, rule := range strings.Split(rules, "|") {

			fieldValue, ok := params[field]

			if rule == "required" && ok == false {
				error = error + " " + field + " is required.\r\n"
			}

			if ok == true {

				if rule == "alphanumeric" {

					re := regexp.MustCompile("^[a-zA-Z0-9 ]*$")

					if re.MatchString(fmt.Sprint(fieldValue)) == false {
						error = error + "The value of '" + field + "' is not a valid alphanumeric value.\r\n"
					}
				}

				if rule == "string" {

					re := regexp.MustCompile("^[a-zA-Z ]*$")

					if re.MatchString(fmt.Sprint(fieldValue)) == false {
						error = error + "The value of '" + field + "' is not a valid string value.\r\n"
					}
				}

				if rule == "hexId" {

					if !bson.IsObjectIdHex(fmt.Sprint(fieldValue)) {
						error = error + "The value of '" + field + "' is not a valid Object hexId value.\r\n"
					}
				}

				if rule == "email" {

					re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

					if re.MatchString(fmt.Sprint(fieldValue)) == false {
						error = error + "The value of '" + field + "' is not a valid emailId.\r\n"
					}
				}

				if rule == "designation" {

					if !(fmt.Sprint(fieldValue) == "SDE" || fmt.Sprint(fieldValue) == "SDE2" || fmt.Sprint(fieldValue) == "MANAGER") {
						error = error + "The value of '" + field + "' is not valid.\r\n"
					}

				}

				if rule == "integer" {

					if _, err := strconv.ParseInt(fmt.Sprint(fieldValue), 10, 64); err != nil {
						error = error + "The value of '" + field + "' is not a valid integer value.\r\n"
					}

				}

				if rule == "float" {

					if _, err := strconv.ParseFloat(fmt.Sprint(fieldValue), 10); err != nil {
						error = error + "The value of '" + field + "' is not a valid float value.\r\n"
					}

				}

			}

		}

	}

	if error != "" {
		return errors.New(error)
	}
	return nil
}

func ValidateId(id string) error {

	//Validation
	params := map[string]interface{}{}
	params["id"] = id
	validationRules := map[string]string{"id": "required|alphanumeric"}
	error := Validate(params, validationRules)
	if error != nil {
		return error
	}
	return nil
}

func ValidateHexId(id string) error {

	//Validation
	params := map[string]interface{}{}
	params["id"] = id
	validationRules := map[string]string{"id": "required|hexId"}
	error := Validate(params, validationRules)
	if error != nil {
		return error
	}
	return nil
}

func ValidateUpdateUserReq(req *pb.UpdateUserRequest) error {

	//Validation
	params := map[string]interface{}{}
	params["userId"] = req.UserId
	params["email"] = req.Email
	validationRules := map[string]string{
		"userId": "required|alphanumeric",
		"email":  "required|email",
	}
	error := Validate(params, validationRules)
	if error != nil {
		return error
	}
	return nil
}

func ValidateCreateUserReq(req *pb.CreateUserRequest) error {

	//Validation
	params := map[string]interface{}{}
	params["firstName"] = req.FirstName
	params["lastName"] = req.LastName
	params["email"] = req.Email
	params["designation"] = req.Designation
	validationRules := map[string]string{
		"firstName":   "required|string",
		"lastName":    "required|string",
		"email":       "required|email",
		"designation": "required|designation",
	}
	error := Validate(params, validationRules)
	if error != nil {
		return error
	}
	return nil
}
