package router

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

)

func StartService() {
	// initializing router
	router := mux.NewRouter()
	// register routes
	RegisterUserRoutes(router)
	// register server
	log.Fatal(http.ListenAndServe(":8080", router))

}
