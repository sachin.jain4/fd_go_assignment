package router

import (
	"go-crud/controllers"

	"github.com/gorilla/mux"

)

func RegisterUserRoutes(router *mux.Router) {

	uc := controllers.NewUserController()
	handleUserRoutes(router, uc)
}

func handleUserRoutes(router *mux.Router, uc *controllers.UserController) {

	router.HandleFunc("/assignment/user/getEncodedRequest", uc.GetEncodedRequest).Methods("GET")
	router.HandleFunc("/assignment/users", uc.GetAllUsers).Methods("GET")
	router.HandleFunc("/assignment/user", uc.GetUserByUserId).Methods("GET")
	router.HandleFunc("/assignment/user", uc.CreateUser).Methods("POST")
	router.HandleFunc("/assignment/user", uc.UpdateUser).Methods("PATCH")
	router.HandleFunc("/assignment/user", uc.DeleteUser).Methods("DELETE")

}
