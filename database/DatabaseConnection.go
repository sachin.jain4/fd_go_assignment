package database

import "gopkg.in/mgo.v2"

type DBConnection struct {
	session *mgo.Session
}

var connection = SetupDbConnection()

func SetupDbConnection() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost:27017")
	if err != nil {
		panic(err)
	}

	return s
}

func GetCollection(collection string) *mgo.Collection {

	return SetupDbConnection().DB("go-crud").C(collection)
}
