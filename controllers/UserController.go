package controllers

import (
	"encoding/base64"
	"fmt"
	"go-crud/service"
	"io/ioutil"
	"log"
	"net/http"
)

type UserController struct{}

func NewUserController() *UserController {
	return &UserController{}
}

func handleErr(err error, msg string) {
	if err != nil {
		log.Println(err, msg)
	}
}

func (uc UserController) GetUserByUserId(w http.ResponseWriter, r *http.Request) {

	data := r.URL.Query().Get("proto_body")

	reqBody, _ := base64.StdEncoding.DecodeString(data)

	user, err := service.GetUserByUserId(reqBody)
	w.Header().Set("Content-Type", "application/x-protobuf")
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "%s", err.Error())
	} else {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "%s", user)
	}
}

func (uc UserController) GetAllUsers(w http.ResponseWriter, r *http.Request) {

	uj, err := service.GetAllUsers()
	w.Header().Set("Content-Type", "application/x-protobuf")
	fmt.Fprintf(w, "%s", uj)
	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request) {

	bodyBytes, err := ioutil.ReadAll(r.Body)
	handleErr(err, "error while reading request data")

	res, err := service.CreateUser(bodyBytes)
	w.Header().Set("Content-Type", "application/x-protobuf")
	fmt.Fprintf(w, "%s \n", res)
	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
		// fmt.Fprintf(w, "%s", "Error while creating user")
	} else {
		w.WriteHeader(http.StatusCreated)
	}
}

func (uc UserController) UpdateUser(w http.ResponseWriter, r *http.Request) {

	bodyBytes, err := ioutil.ReadAll(r.Body)
	handleErr(err, "error while reading request data")

	res, err := service.UpdateUser(bodyBytes)
	w.Header().Set("content-type", "application/x-protobuf")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s", res)
	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
	}
}

func (uc UserController) DeleteUser(w http.ResponseWriter, r *http.Request) {

	bodyBytes, err := ioutil.ReadAll(r.Body)
	handleErr(err, "error while reading request data")

	res, err := service.DeleteUser(bodyBytes)
	w.Header().Set("Content-Type", "application/x-protobuf")
	fmt.Fprintf(w, "%s", res)
	if err != nil {
		w.WriteHeader(http.StatusBadGateway)
		// fmt.Fprintf(w, "%s", "Error while deleting user")
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

func (uc UserController) GetEncodedRequest(w http.ResponseWriter, r *http.Request) {

	id := r.Header.Get("id")
	base64EncodedReq, err := service.GetEncodedRequest(id)
	w.Header().Set("Content-Type", "application/x-protobuf")
	fmt.Fprintf(w, "%s", base64EncodedReq)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	} else {
		w.WriteHeader(http.StatusOK)
	}

}
