package dao_test

import (
	"go-crud/dao"
	"go-crud/models"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/mgo.v2/bson"

)

func TestSaveUser(t *testing.T) {
	assert := assert.New(t)

	u := models.User{}
	u.Id = bson.ObjectIdHex("62ef5b19ff4c60ea745ed7ab")
	u.FirstName = "Test"
	u.LastName = "Case"
	u.Email = "TestC@gmail.com"
	err := dao.SaveUser(u)

	assert.Equal(err, nil)
}

func TestGetUserById(t *testing.T) {
	assert := assert.New(t)
	expected := "62ef5b19ff4c60ea745ed7ab"
	id := bson.ObjectIdHex(expected)
	u, _ := dao.GetUserById(id)
	assert.Equal(expected, u.Id.Hex())
}

func TestGetAllUsers(t *testing.T) {
	assert := assert.New(t)
	_, err := dao.GetAllUsers()
	assert.Equal(err, nil)
}

func TestUpdateUser(t *testing.T) {
	assert := assert.New(t)

	id := bson.ObjectIdHex("62ef5b19ff4c60ea745ed7ab")
	u := models.User{}
	u.Id = id
	u.FirstName = "Test"
	u.LastName = "Case"
	u.Email = "TestC@gmail.com"
	err := dao.UpdateUser(id, u)

	assert.Equal(err, nil)
}

func TestDeleteUser(t *testing.T) {
	assert := assert.New(t)

	id := bson.ObjectIdHex("62ef5b19ff4c60ea745ed7ab")
	err := dao.DeleteUser(id)

	assert.Equal(err, nil)
}
