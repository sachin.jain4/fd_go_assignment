package dao_test

import (
	"go-crud/dao"
	"go-crud/models"
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/mgo.v2/bson"
)

func TestSaveEmp(t *testing.T) {
	assert := assert.New(t)

	e := models.Employee{}
	e.Id = bson.ObjectIdHex("62ef5b19ff4c60ea745ed7ab")
	e.UserId = "TestC"
	e.Designation = "SDE"
	err := dao.SaveEmp(e)

	assert.Equal(err, nil)
}

func TestGetEmpById(t *testing.T) {
	assert := assert.New(t)

	expected := "62ef5b19ff4c60ea745ed7ab"
	id := bson.ObjectIdHex(expected)
	u, _ := dao.GetEmpById(id)

	assert.Equal(expected, u.Id.Hex())
}

func TestFindEmpByUserId(t *testing.T) {
	assert := assert.New(t)

	e := models.Employee{}
	e.Id = bson.ObjectIdHex("62ef5b19ff4c60ea745ed7ab")
	e.UserId = "TestC"
	e.Designation = "SDE"

	userId := "TestC"
	emp, err := dao.FindEmpByUserId(userId)

	assert.Equal(e.Id.Hex(), emp.Id.Hex())
	assert.Equal(e.UserId, emp.UserId)
	assert.Equal(e.Designation, emp.Designation)
	assert.Equal(err, nil)
}

func TestDeleteEmp(t *testing.T) {
	assert := assert.New(t)

	id := bson.ObjectIdHex("62ef5b19ff4c60ea745ed7ab")
	err := dao.DeleteEmp(id)

	assert.Equal(err, nil)
}
