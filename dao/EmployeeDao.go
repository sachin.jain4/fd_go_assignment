package dao

import (
	"go-crud/database"
	"go-crud/models"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

)

var employee *mgo.Collection = database.GetCollection("employee")

func GetEmpById(id bson.ObjectId) (*models.Employee, error) {
	e := models.Employee{}
	err := employee.FindId(id).One(&e)
	return &e, err
}

func FindEmpByUserId(id string) (*models.Employee, error) {
	e := models.Employee{}
	err := employee.Find(bson.M{"userId": id}).One(&e)
	return &e, err
}

func SaveEmp(emp models.Employee) error {
	return employee.Insert(emp)
}

func DeleteEmp(id bson.ObjectId) error {
	return employee.RemoveId(id)
}
