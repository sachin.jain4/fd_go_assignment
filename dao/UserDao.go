package dao

import (
	"go-crud/database"
	"go-crud/models"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

)

var users *mgo.Collection = database.GetCollection("users")

func GetUserById(id bson.ObjectId) (*models.User, error) {
	e := models.User{}
	err := users.FindId(id).One(&e)
	return &e, err
}

func GetAllUsers() ([]models.User, error) {
	usrs := []models.User{}
	err := users.Find(nil).All(&usrs)
	return usrs, err
}

func SaveUser(user models.User) error {
	err := users.Insert(user)
	return err
}

func UpdateUser(id bson.ObjectId, user models.User) error {
	err := users.UpdateId(id, &user)
	return err
}

func DeleteUser(id bson.ObjectId) error {
	return users.RemoveId(id)
}
